<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('/login/', 'AuthenticationController@login');
Route::post('/login/', 'AuthenticationController@login');

Route::get('/logout/', 'AuthenticationController@logout');

Route::get('/register/', 'RegistrationController@index');
Route::post('/register/', 'RegistrationController@index');

Route::group(array('prefix'=>'wallet', 'before'=> 'auth'), function(){
    Route::get('/', 'WalletsController@index');
    Route::get('/add/', 'WalletsController@add');
    Route::post('/add/', 'WalletsController@add');
    Route::get('/send/', 'WalletsController@send');
    Route::post('/send/', 'WalletsController@send');
    Route::get('/transactions/','WalletsController@transactions');
    Route::get('/withdraw/','WalletsController@withdraw');
    Route::post('/withdraw/','WalletsController@withdraw');
});

Route::group(array('before'=>'auth', 'prefix'=>'bankaccounts'), function () {
    Route::get('/', 'WalletsController@list_bank_accounts');
    Route::get('/add/', 'WalletsController@add_bank_account');
    Route::post('/add/', 'WalletsController@add_bank_account');
});

Route::resource('wallets', 'WalletsController');
