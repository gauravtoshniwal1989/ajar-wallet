@extends('base')

@section('content')
    @if($errors)
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    {{ Form::open(array('action' => 'WalletsController@send')) }}
    {{ Form::label('email', 'E-Mail Address') }}
    {{ Form::text('to_email') }}
    <br>
    {{ Form::label('amount', 'amount') }}
    {{ Form::text('amount') }}
    <br>
    {{ Form::submit('Send Money') }}
    {{ Form::close() }}
@stop