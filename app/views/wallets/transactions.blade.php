@extends('base')

@section('content')
    <table>
        <tr>
            <th>
                To/From Email
            </th>
            <th>
                Date
            </th>
            <th>
                Credit
            </th>
        </tr>
        @foreach ($transactions as $transaction)
            <tr>
                <td>
                    @if($transaction->from_email == $user->email)
                        {{ $transaction->to_email }}
                    @else
                        {{ $transaction->from_email }}
                    @endif
                </td>
                <td>
                    {{ $transaction->created_at }}
                </td>
                <td>
                    @if($transaction->to_email == $user->email)
                        + {{ $transaction->amount }}
                    @elseif($transaction->from_user_id == $user->id)
                        - {{ $transaction->amount }}
                    @endif
                </td>
            </tr>
        @endforeach
    </table>
@stop