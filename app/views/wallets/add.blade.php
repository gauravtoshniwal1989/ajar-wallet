@extends('base')

@section('content')
    @if($errors)
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    {{ Form::open(array('action' => 'WalletsController@add')) }}
    {{ Form::label('amount', 'Amount') }}
    {{ Form::text('amount') }}
    <br>
    {{ Form::submit('Add Money') }}
    {{ Form::close() }}
@stop