@extends('base')

@section('content')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    Welcome {{ $user->name }}. Your wallet balance is {{ $user->wallet->balance }}. <br>
    <a href='{{ url('/wallet/add/') }}' style="-webkit-appearance: button;-moz-appearance: button;appearance: button;">Add Balance</a> |
    <a href='{{ url('/wallet/send/') }}' >Send Money</a> |
    <a href='{{ url('/wallet/transactions/') }}'>Transactions</a> |
    <a href='{{ url('/wallet/withdraw/') }}' >Withdraw money</a> |
@stop