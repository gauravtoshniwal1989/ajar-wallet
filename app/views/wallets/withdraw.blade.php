@extends('base')

@section('content')
    Total Balance: {{ $user->wallet->balance }}
    @if($errors)
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    {{ Form::open(array('action' => 'WalletsController@withdraw')) }}
    {{ Form::label('amount', 'Amount') }}
    {{ Form::text('amount') }}
    <br>
    {{ Form::label('bank_account_id', 'Bank Account') }}
    {{ Form::select('bank_account_id', $accounts) }}
    <br>
    {{ Form::submit('Send Money') }}
    {{ Form::close() }}
@stop