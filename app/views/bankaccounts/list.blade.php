@extends('base')

@section('content')
    <table>
        <tr>
            <th>
                Bank
            </th>
            <th>
                Account Number
            </th>
        </tr>
        @foreach ($bankaccounts as $bankaccount)
            <tr>
                <td>
                    {{ $bankaccount->bank }}
                </td>
                <td>
                    {{ $bankaccount->account_number }}
                </td>
            </tr>
        @endforeach
    </table>
    <a class="btn btn-primary" href="/bankaccounts/add/">Add Bank Account</a>
@stop