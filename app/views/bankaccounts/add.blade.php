@extends('base')

@section('content')
    @if($errors)
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif
    {{ Form::open(array('action' => 'WalletsController@add_bank_account')) }}
    {{ Form::label('bank', 'Bank Name') }}
    {{ Form::text('bank') }}
    <br>
    {{ Form::label('account_number', 'Account Number') }}
    {{ Form::text('account_number') }}
    <br>
    {{ Form::submit('Add Account') }}
    {{ Form::close() }}
@stop