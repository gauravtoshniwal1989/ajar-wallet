@extends('base')

@section('content')
    {{ Form::open(array('action' => 'AuthenticationController@login')) }}
    {{ Form::label('email', 'E-Mail Address') }}
    {{ Form::text('email') }}
    {{ Form::label('password', 'Password') }}
    {{ Form::password('password') }}
    {{ Form::submit('Login') }}
    {{ Form::close() }}
@stop