@extends('base')

@section('content')
    @if($errors)
        <ul>
        @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
        @endforeach
        </ul>
    @endif
    {{ Form::open(array('action' => 'RegistrationController@index')) }}
    {{ Form::label('email', 'E-Mail Address') }}
    {{ Form::text('email') }}
    <br>
    {{ Form::label('password', 'Password') }}
    {{ Form::password('password') }}
    <br>
    {{ Form::label('name', 'Name') }}
    {{ Form::text('name') }}
    <br>
    {{ Form::submit('Register') }}
    {{ Form::close() }}
@stop