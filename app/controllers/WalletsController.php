<?php

class WalletsController extends \BaseController {

    protected $layout = 'base';

    protected $request;

    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->request = $request;
    }

    public function index()
	{
	    $user = Auth::user();
        return View::make('wallets.index', array('user'=>$user));
	}


	public function add()
	{
        $user = Auth::user();
        if ($this->request->isMethod('post')) {
            $rules = array(
                'amount' => 'required|integer'
            );

            $validator = Validator::make(Input::all(), $rules);

            if (!$validator->fails()) {
                $amount = (int)Input::get('amount');
                DB::transaction(function () use ($user, $amount) {
                    $transaction = Transaction::create(array(
                        'amount'=> $amount,
                        'email'=> $user->email,
                        'user_id'=>$user->id,
                        'status'=>'REALIZED'
                    ));
                    DB::table('wallets')->where('id',$user->wallet->id)->increment('balance', $amount);
                }, 5);
                return Redirect::intended('wallet');

            }
        }
        return View::make('wallets.add', array('user'=>$user));
	}



	public function send()
    {
        $from_user = Auth::user();
        if ($this->request->isMethod('post'))
        {
            $rules = array(
                'amount' => 'required|integer|max:'.$from_user->wallet->balance,
                'to_email' => 'required|email'
            );

            $validator = Validator::make(Input::all(), $rules);

            if (!$validator->fails())
            {
                $amount = (int)Input::get('amount');
                $to_email = Input::get('to_email');
                $to_user = User::whereEmail($to_email)->first();
                $transaction = DB::transaction( function () use ($amount, $to_email, $to_user, $from_user) {
                    $transaction = Transaction::create(array(
                        'amount'=> $amount,
                        'to_email'=> $to_email,
                        'to_user_id' => $to_user ? $to_user->id:null,
                        'from_user_id'=> $from_user->id,
                        'from_email'=> $from_user->email,
                        'status'=> $to_user ? 'REALIZED' : 'ADDED'
                    ));

                    // If the given User ID already exists in the system,
                    // add amount to the recipient and subtract from the sender
                    //
                    if ($to_user) {
                        DB::table('wallets')->where('id',$to_user->wallet->id)->increment('balance', $amount);
                    }
                    // Subtract from the sender balance immediately.
                    // It shall be a periodic process to re-add subtracted but not claimed transfers after a few days
                    DB::table('wallets')->where('id',$from_user->wallet->id)->decrement('balance', $amount);
                    return $transaction;
                });
                if ($transaction) {
                    $message = $to_user? "Money has been transferred.":"User does not exist with this email ID. Whenever the user registers, money will be transferred.";
                    Session::flash('message', $message);
                    return Redirect::intended('wallet');
                }
                else
                {
                    Session::flash('message', "Some erorr occoured.");
                    return Redirect::intended('wallet');
                }
            }
            else
            {
                return Redirect::intended('wallet/send')->withInput()->withErrors($validator);
            }

        }
        else
        {
            return View::make('wallets.send');
        }
    }

    public function transactions()
    {
        $user = Auth::user();
        $transactions = Transaction::where('to_email', $user->email)->orWhere('from_user_id', $user->id)->get();
        return View::make('wallets.transactions', array('transactions'=>$transactions, 'user'=>$user));
    }

    public function withdraw()
    {
        $user = Auth::user();
        if ($this->request->isMethod('post'))
        {
            $rules = array(
                'amount' => 'required|integer|max:'.$user->wallet->balance
            );
            $validator = Validator::make(Input::all(), $rules);

            if (!$validator->fails())
            {
                $transaction = Transaction::create(array(
                    'amount'=> Input::get('amount'),
                    'from_user_id'=>$user->id,
                    'from_email'=> $user->email,
                    'to_user_id' => $user->id,
                    'to_email' => $user->email,
                    'status'=>'REALIZED'
                ));
                DB::table('wallets')->where('id',$user->wallet->id)->decrement('balance', Input::get('amount'));
                return Redirect::intended('wallet');
            }
            else{
                return View::make('wallets.withdraw')->withInput()->withErrors($validator);
            }
        }
        else
        {
            $accounts = BankAccount::where('user_id', $user->id)->lists('bank','id');
            return View::make('wallets.withdraw', array('user'=>$user, 'accounts'=>$accounts));
        }
    }

    public function add_bank_account()
    {
        $user = Auth::user();
        if ($this->request->isMethod('post'))
        {
            $bankaccount = BankAccount::create(array(
                'bank' => Input::get('bank'),
                'account_number'=> Input::get('account_number'),
                'user_id'=>$user->id
            ));
            return Redirect::intended('bankaccounts/');
        }
        else
        {
            return View::make('bankaccounts.add', array('user'=>$user));
        }
    }

    public function list_bank_accounts()
    {
        $user = Auth::user();
        $bankaccounts = $user->accounts()->get();
        return View::make('bankaccounts.list', array('bankaccounts'=>$bankaccounts, 'user'=>$user));
    }
}