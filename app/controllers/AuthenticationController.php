<?php
class AuthenticationController extends BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /authenticationconroller
	 *
	 * @return Response
	 */


	protected $request;

    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->request = $request;
    }

    public function login()
	{
        if ($this->request->isMethod('post'))
        {
            $email = $this->request->get('email');
            $password = $this->request->get('password');
            if (Auth::attempt(array('email' => $email, 'password' => $password)))
            {
                return Redirect::intended('wallet');
            }
        }
        else {
            return View::make('authentication.login');
        }
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /authenticationconroller/create
	 *
	 * @return Response
	 */
	public function logout()
	{
		return "Logout";
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /authenticationconroller
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /authenticationconroller/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /authenticationconroller/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /authenticationconroller/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /authenticationconroller/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}