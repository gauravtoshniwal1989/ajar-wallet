<?php

class RegistrationController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /registration
	 *
	 * @return Response
	 */
    protected $request;

    public function __construct(\Illuminate\Http\Request $request)
    {
        $this->request = $request;
    }

	public function index()
	{
        if ($this->request->isMethod('post'))
        {
            $rules =     array(
                'name' => 'required',
                'password' => 'required|min:6',
                'email' => 'required|email|unique:users'
            );

            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails())
            {
                return Redirect::to('register')->withInput()->withErrors($validator);
            }
            else
            {
                $user = DB::Transaction(function(){
                    $temp_user = User::create(array(
                        'email'=>Input::get('email'),
                        'password'=>Hash::make(Input::get('password')),
                        'name'=>Input::get('name')
                    ));
                    $wallet = new Wallet();

                    // Get the total amount to be added to this new user's wallet
                    // Also update the status of all the related records
                    $total_amount = Transaction::whereToEmail(Input::get('email'))->whereStatus('ADDED')->get()->sum('amount');
                    $transactions_to_be_updated = Transaction::whereToEmail(Input::get('email'))->whereStatus('ADDED')->update(['status'=>'REALIZED']);

                    // Update the wallet balance with the amount obtained from transactions
                    $wallet->balance = $total_amount?$total_amount:0;
                    $temp_user->wallet()->save($wallet);

                    $temp_user = array(
                        'email' => Input::get('email'),
                        'password' => Input::get('password')
                    );
                    // Get all the entries in the Transactions that are associated with the email ID
                    // Get a total of these entries and assign to the wallet
                    return $temp_user;
                });
                if (Auth::attempt($user))
                {
                    return Redirect::intended('wallet');
                }
            }
        }
        else {
            return View::make('register');
        }
	}
}