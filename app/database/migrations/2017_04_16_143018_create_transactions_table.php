<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function ($table) {
		    $table->increments('id');
            $table->integer('amount');
            $table->string('to_email')->nullable();
            $table->string('from_email')->nullable();
            $table->enum('status', array('ADDED', 'REALIZED', 'CANCELLED'))->default('ADDED');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('from_user_id')->unsigned()->nullable();
            $table->foreign('from_user_id')->references('id')->on('users');
            $table->integer('to_user_id')->unsigned()->nullable();
            $table->foreign('to_user_id')->references('id')->on('users');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}

}
