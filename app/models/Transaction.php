<?php

class Transaction extends \Eloquent {
	protected $fillable = [
	    'amount',
        'to_email',
        'to_user_id',
        'from_user_id',
        'from_email',
        'status'
    ];

    public function to_user()
    {
        return $this->belongsTo('User');
    }
    public function from_user()
    {
        return $this->belongsTo('User');
    }

}