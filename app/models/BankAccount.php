<?php

class BankAccount extends \Eloquent {
	protected $fillable = ['bank', 'account_number', 'user_id'];

    public function user()
    {
        return $this->belongsTo('User');
    }
}