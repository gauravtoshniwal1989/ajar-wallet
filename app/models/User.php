<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

    public function wallet()
    {
        return $this->hasOne('Wallet');
    }

    public function accounts()
    {
        return $this->hasMany('BankAccount');
    }

	protected $fillable = array(
	    'email',
        'password',
        'name'
    );
	protected $hidden = array('password', 'remember_token');

}
